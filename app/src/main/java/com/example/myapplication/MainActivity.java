package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startprofile(View view) {
        Intent intent = new Intent(this,about.class);
        startActivity(intent);
    }

    public void btn_contact(View view) {
        Intent intent1 = new Intent(this,contact.class);
        startActivity(intent1);
    }
}