package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class contact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
    }

    public void btn_back1(View view) {
        Intent intent2 = new Intent(this,MainActivity.class);
        startActivity(intent2);
    }

    public void run_fb(View view) {
        Intent intent3 = new  Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/nla.patiparn/"));
        startActivity(intent3);
    }

    public void run_ig(View view) {
        Intent intent4 = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.instagram.com/la_seedanoi/?hl=en"));
        startActivity(intent4);
    }
}